lavasearx
=========

A privacy-respecting, hackable `metasearch
engine <https://en.wikipedia.org/wiki/Metasearch_engine>`__.

Based on `Searx <https://github.com/asciimoo/searx>`__, this is a fork dedicated to open sourcing the changes of LavaTech team.

Public instance at `searx.lavatech.top <https://searx.lavatech.top>`__.

See searx' `documentation <https://asciimoo.github.io/searx>`__ and the `wiki <https://github.com/asciimoo/searx/wiki>`__ for more information.


Installation
~~~~~~~~~~~~

With Docker
------
Go to the `searx-docker <https://github.com/searx/searx-docker>`__ project.

Without Docker
------
For all the details, follow this `step by step installation <https://asciimoo.github.io/searx/dev/install/installation.html>`__.

Note: the documentation needs to be updated.

If you are in hurry
------
-  clone source:
   ``git clone https://gitlab.com/lavatech/lavasearx.git && cd searx``
-  install dependencies: ``./manage.sh update_packages``
-  edit your
   `settings.yml <https://github.com/asciimoo/searx/blob/master/searx/settings.yml>`__
   (set your ``secret_key``!)
-  run ``python searx/webapp.py`` to start the application


`License <https://gitlab.com/lavatech/lavasearx/blob/master/LICENSE>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

More about searx
~~~~~~~~~~~~~~~~

-  `github <https://github.com/asciimoo/searx>`__
-  `openhub <https://www.openhub.net/p/searx/>`__
-  `twitter <https://twitter.com/Searx_engine>`__
-  IRC: #searx @ freenode

