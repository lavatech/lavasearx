from os import listdir
from os.path import realpath, dirname, join, isdir
from sys import version_info
from searx.utils import load_module
from collections import defaultdict

if version_info[0] == 3:
    unicode = str


answerers_dir = dirname(realpath(__file__))


def load_answerers():
    answerers = []
    for filename in listdir(answerers_dir):
        if not isdir(join(answerers_dir, filename)) or filename.startswith('_'):
            continue
        module = load_module('answerer.py', join(answerers_dir, filename))
        if not hasattr(module, 'keywords') or not isinstance(module.keywords, tuple) or not len(module.keywords):
            exit(2)
        answerers.append(module)
    return answerers


def get_answerers_by_keywords(answerers):
    by_keyword = defaultdict(list)
    for answerer in answerers:
        for keyword in answerer.keywords:
            for keyword in answerer.keywords:
                by_keyword[keyword].append(answerer.answer)
    return by_keyword


def check_for_bangs(query_parts):
    start_word = query_parts[0].decode('utf-8')
    end_word = query_parts[-1].decode('utf-8')

    bang = "!"

    # hahahahha this is so bad
    return start_word[0] == bang or start_word[-1] == bang or\
        end_word[0] == bang or end_word[-1] == bang


def ask(query):
    results = []
    query_parts = list(filter(None, query.query.split()))

    # TODO: make this less hacky, move to regexes instead

    start_word = query_parts[0].decode('utf-8')

    if start_word not in answerers_by_keywords and\
            not check_for_bangs(query_parts):
        return results

    # oh my god this is so hacky
    if start_word in answerers_by_keywords:
        for answerer in answerers_by_keywords[start_word]:
            result = answerer(query)
            if result:
                results.append(result)

    if check_for_bangs(query_parts):
        answerer = answerers_by_keywords["bang"][0]
        result = answerer(query)
        if result:
            results.append(result)

    return results


answerers = load_answerers()
answerers_by_keywords = get_answerers_by_keywords(answerers)
