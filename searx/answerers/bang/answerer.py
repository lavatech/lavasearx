from sys import version_info

from flask_babel import gettext
from searx.url_utils import quote

if version_info[0] == 3:
    unicode = str

# These are ignored
keywords = ('!g',
            'bang',
            'g!',)


# required answerer function
# can return a list of results (any result type) for a given query
def answer(query):
    arg = query.query

    # parts = query.query.split()
    # try:
    #     # TODO: Make this less hacky
    #     if parts[1] in keywords:
    #         args = [part.decode() for part in parts[1:]]
    #     else:
    #         args = [part.decode() for part in parts[:-1]]
    # except:
    #     return [{'redirect': "https://google.com"}]
    # return [{'redirect': urlencode("https://google.com/search?q=" + " ".join(args))}]

    # Uses https://duckduckgo.com/api
    # Could be modified to use stuff locally, see the code I used to use above
    # DDG's bang store is on https://duckduckgo.com/bang.js
    # Keep in mind that This Does Not Scale: https://acab.fyi/i/qkwcvc0p.png
    return [{'redirect': "https://api.duckduckgo.com/?q={}".format(quote(arg))}]


# required answerer function
# returns information about the answerer
def self_info():
    return {'name': gettext('Bangs'),
            'description': gettext('Does DDG-style redirection bangs'),
            'examples': ['!g lavatech']}
